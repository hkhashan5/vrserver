﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace VrDMIServer.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values

        public HttpResponseMessage Get(String id)
        {
            if (id == "LeaderBoard")
            {
                var stream = new FileStream(@"C:\Users\Delegate\Desktop\VSServer\VrDMIServer\VrDMIServer\JSON\Leader_Board.json", FileMode.Open);
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            } else if(id == "MyMetrices")
            {
                var stream = new FileStream(@"C:\Users\Delegate\Desktop\VSServer\VrDMIServer\VrDMIServer\JSON\MyMetrices.json", FileMode.Open);
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;

            }else if (id == "DMI")
            {
                var stream = new FileStream(@"C:\Users\Delegate\Desktop\VSServer\VrDMIServer\VrDMIServer\JSON\Dmi.json", FileMode.Open);
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                return result;
            } else
            {
                return null;
            }
        }


        /*
        public string Get(int id)
        {
            var stream = new FileStream(@"C:\Users\Delegate\Desktop\VSServer\VrDMIServer\VrDMIServer\JSON\Era.json", FileMode.Open);

            var result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return result;
        }
        */
        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
