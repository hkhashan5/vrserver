﻿using GetRequestTest01.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace GetRequestTest01.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            string uri = String.Format("http://localhost:52830/api/values");
            WebRequest requestObject = WebRequest.Create(uri);
            requestObject.Method = "GET";
            HttpWebResponse responseObject = null;
            responseObject = (HttpWebResponse)requestObject.GetResponse();

            string strresulttest = null;
            using (Stream stream = responseObject.GetResponseStream())
            {
                StreamReader sr = new StreamReader(stream);
                strresulttest = sr.ReadToEnd();
                sr.Close();

                Nps nps = new Nps(strresulttest);

                string name = nps.name;
                string target = nps.target;
                string measure = nps.measure;
                string promoters = nps.promoters;
                string detractors = nps.detractors;
                string passiv = nps.passive;
                string notRecived = nps.notRecived;


            }

            /*
            var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            //List<VoiceOfClient> objList = (List<VoiceOfClient>)serializer.Deserialize(strresulttest, typeof(List<VoiceOfClient>));

            List<Nps> nps = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Nps>>(strresulttest);
        
            
            foreach (Nps item in nps[0].name)
            {
                string name = item.name;
                string target = item.target;
                string measure = item.measure;
                string promoters = item.promoters;
                string detractors = item.detractors;
                string passiv = item.passive;
                string notRecived = item.notRecived;
                /*
                string negative = item.negative;
                string positive = item.positive;
                string releasesMet = item.releasesMet;
                string releasesNotMet = item.releasesNotMet;
                string releasesNoData = item.releasesNoData;
                
            }
            
                string name = nps[0].name;
                string target = nps[0].target;
                string measure = nps[0].measure;
                string promoters = nps[0].promoters;
                string detractors = nps[0].detractors;
                string passiv = nps[0].passive;
                string notRecived = nps[0].notRecived;
            */
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}