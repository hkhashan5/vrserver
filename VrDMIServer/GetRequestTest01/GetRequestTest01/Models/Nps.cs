﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace GetRequestTest01.Models
{
    public class Nps
    {
        public Nps(string json)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var jsonObject = serializer.Deserialize<dynamic>(json);
            //JObject jsonObject = JObject.Parse(json);
            //JToken jNps = jsonObject["NPS"];
            name = (string)jsonObject[0]["name"];
            target = (string)jsonObject[0]["target"];
            measure = (string)jsonObject[0]["measure"];
            promoters = (string)jsonObject[0]["promoters"];
            detractors = (string)jsonObject[0]["detractors"];
            passive = (string)jsonObject[0]["passive"];
            notRecived = (string)jsonObject[0]["notRecived"];
        }
        public string name { get; set; }
        public string target { get; set; }
        public string measure { get; set; }
        public string promoters { get; set; }
        public string detractors { get; set; }
        public string passive { get; set; }
        public string notRecived { get; set; }
    }
}