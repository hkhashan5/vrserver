﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GetRequestTest01.Models
{
    public class VoiceOfClient
    {
        public string name { get; set; }
        public string target { get; set; }
        public string measure { get; set; }
        public string promoters { get; set; }
        public string detractors { get; set; }
        public string passive { get; set; }
        public string notRecived { get; set; }
        public string negative { get; set; }
        public string positive { get; set; }
        public string releasesMet { get; set; }
        public string releasesNotMet { get; set; }
        public string releasesNoData { get; set; }
    }
}